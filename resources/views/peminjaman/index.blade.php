@extends('layout.master')

@section('content')
<a href="{{url('/Peminjaman')}}">
<button type="button" class="btn btn-primary">tambah</button>
</a>
 <!-- /.card-header -->
 <div class="card-body">
    <table id="datatable" class="table table-bordered table-hover">
      <thead>
      <tr>
        <th>Id</th>
        <th>Member Id</th>
        <th>Denda</th>
        <th>Tanggal Pinjam</th>
        <th>Tanggal Kembali</th>
        <th>Status</th>        
      </tr>
      </thead>     
      <tbody>
        <?php $i = 1; ?>
            @foreach($peminjaman as $peminjaman)
            <tr>
                <td class="text-center">{{ $i++ }}</td>
                <td>{{$peminjaman->id}}</td>
                <td>{{$peminjaman->member_id}}</td>
                <td>{{$peminjaman->denda}}</td>
                <td>{{$peminjaman->tanggal_pinjam}}</td>
                <td>{{$peminjaman->tanggal_kembali}}</td>
                <td>{{$peminjaman->status}}</td>
                <td>
                  <a href="{{url('/buku/edit', $buku->id) }}">
                    <button type="button" class="btn btn-primary">edit</button>
                  </a>
                  <a href="{{url('/buku/delete', $buku->id) }}">
                    <button type="button" class="btn btn-primary">delete</button>
                  </a>
            </tr>
            @endforeach
    </tbody>      
    </table>
  </div>

@endsection