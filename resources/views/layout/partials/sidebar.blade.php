<!-- Sidebar -->
<div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block">Alexander Pierce</a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item has-treeview">
            <a href="{{ url('/dashboard')}}" class="nav-link">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>
                    Dashboard
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
        </li>
        <li class="nav-item has-treeview">
          <a href="{{ url('/buku')}}" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                  Buku
                  <i class="right fas fa-angle-left"></i>
              </p>
          </a>
        </li>
        <li class="nav-item has-treeview">
            <a href="{{ url('/KategoriBuku')}}" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Kategori Buku
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
          </li>
        <li class="nav-item has-treeview">
          <a href="{{ url('/peminjaman')}}" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                  Peminjaman
                  <i class="right fas fa-angle-left"></i>
              </p>
          </a>
        </li>
        <li class="nav-item has-treeview">
          <a href="{{ url('/detailpeminjaman')}}" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                  Detail Peminjaman
                  <i class="right fas fa-angle-left"></i>
              </p>
          </a>
        </li>        
        <li class="nav-item has-treeview">
          <a href="{{ url('/user')}}" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                  User
                  <i class="right fas fa-angle-left"></i>
              </p>
          </a>
        </li>
        <li class="nav-item has-treeview">
          <a href="{{ url('/member')}}" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                  Member
                  <i class="right fas fa-angle-left"></i>
              </p>
          </a>
        </li>
        <li class="nav-item has-treeview">
          <a href="{{ route('logout') }}"
          onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                  Logout
                  <i class="right fas fa-angle-left"></i>
              </p>
          </a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
          </form>
        </li>
        </ul>
    </nav>
  </div>