@extends('layout.master')

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Quick Example</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form role="form" method="post" action="{{url("/KategoriBuku/edit", $kategori->id)}}">
    {{ csrf_field() }}
      <div class="box-body">
        <div class="form-group">
            <div class="form-group">
                <label for="exampleInputEmail1">Nama</label>
                <input type="text" name="nama" class="form-control" id="exampleInputtext1" value="{{$kategori->nama}}" placeholder="Nama">
            </div>
            
      <!-- /.box-body -->

      <div class="box-footer">
        <button type="submit" class="btn btn-primary">GANTI</button>
      </div>
    </form>
  </div>

  @endsection