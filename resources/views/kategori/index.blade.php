@extends('layout.master')

@section('content')
<a href="{{url('/KategoriBuku/create')}}">
<button type="button" class="btn btn-primary">Tambah</button>
</a>
 <!-- /.card-header -->
 <div class="card-body">
    <table id="datatable" class="table table-bordered table-hover">
      <thead>
      <tr>
        <th>Id</th>
        <th>Nama</th>
        <th>Action</th>
      </tr>
      </thead>     
      <tbody>
        <?php $i = 1; ?>
            @foreach($kategori as $kategori)
            <tr>
                <td class="text-center">{{ $i++ }}</td>                
                <td>{{$kategori->nama}}</td>                
                <td>
                  <a href="{{url('/KategoriBuku/edit', $kategori->id) }}">
                    <button type="button" class="btn btn-primary">Edit</button>
                  </a>
                  <a href="{{url('/KategoriBuku/delete', $kategori->id) }}">
                    <button type="button" class="btn btn-primary">Delete</button>
                  </a>
            </tr>
            @endforeach
    </tbody>      
    </table>
  </div>

@endsection