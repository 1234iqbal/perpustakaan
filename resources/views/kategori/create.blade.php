@extends('layout.master')

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Pengisian</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form role="form" method="post" action="{{url("/KategoriBuku/create")}}">
    {{ csrf_field() }}
    <div class="box-body">
        <div class="form-group">
          <label for="exampleInputEmail1">Nama</label>
          <input type="text" name="nama" class="form-control" id="exampleInputtext1" placeholder="Nama">
        </div>        
    </div>        
      <!-- /.box-body -->

      <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
  </div>

  @endsection