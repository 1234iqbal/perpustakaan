@extends('layout.master')

@section('content')
<a href="{{url('/buku/create')}}">
<button type="button" class="btn btn-primary">tambah</button>
</a>
 <!-- /.card-header -->
 <div class="card-body">
    <table id="datatable" class="table table-bordered table-hover">
      <thead>
      <tr>
        <th>Nomor</th>
        <th>Gambar</th>
        <th>Judul</th>
        <th>Deskripsi</th>
        <th>Pengarang</th>
        <th>Penerbit</th>
        <th>Tahun Terbit</th>
        <th>Persediaan</th>
        <th>Kategori Id</th>
        <th>Operator id</th>
        <th>Action</th>
      </tr>
      </thead>     
      <tbody>
        <?php $i = 1; ?>
            @foreach($buku as $buku)
            <tr>
                <td class="text-center">{{ $i++ }}</td>
                <td>{{$buku->gambar}}</td>
                <td>{{$buku->judul}}</td>
                <td>{{$buku->deskripsi}}</td>
                <td>{{$buku->pengarang}}</td>
                <td>{{$buku->penerbit}}</td>
                <td>{{$buku->tahun_terbit}}</td>
                <td>{{$buku->persediaan}}</td>
                <td>{{$buku->nama}}</td>
                <td>{{$buku->operator_id}}</td>
                <td>
                  <a href="{{url('/buku/edit', $buku->id) }}">
                    <button type="button" class="btn btn-primary">edit</button>
                  </a>
                  <a href="{{url('/buku/delete', $buku->id) }}">
                    <button type="button" class="btn btn-primary">delete</button>
                  </a>
            </tr>
            @endforeach
    </tbody>      
    </table>
  </div>

@endsection