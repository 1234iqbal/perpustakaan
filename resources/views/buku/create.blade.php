@extends('layout.master')

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Quick Example</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form role="form" method="post" action="{{url("/buku/create")}}">
    {{ csrf_field() }}
      <div class="box-body">
        <div class="form-group">
          <label for="exampleInputEmail1">Gambar</label>
          <input type="text" name="gambar" class="form-control" id="exampleInputtext1" placeholder="Gambar">
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Judul</label>
          <input type="text" name="judul" class="form-control" id="exampleInputPassword1" placeholder="Input Judul">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Deskripsi</label>
            <input type="text" name="deskripsi" class="form-control" id="exampleInputPassword1" placeholder="Input Deskripsi">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Pengarang</label>
            <input type="text" name="pengarang" class="form-control" id="exampleInputPassword1" placeholder="Input Pengarang">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Penerbit</label>
            <input type="text" name="penerbit" class="form-control" id="exampleInputPassword1" placeholder="Input Penerbit">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Tahun Terbit</label>
            <input type="date" name="tahun_terbit" class="form-control" id="exampleInputPassword1" placeholder="Input Tahun Terbit">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Persediaan</label>
            <input type="text" name="persediaan" class="form-control" id="exampleInputPassword1" placeholder="Input Persediaan">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Kategori id</label>
            <input type="text" name="kategori_id" class="form-control" id="exampleInputPassword1" placeholder="Input Persediaan">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Operator id</label>
            <input type="text" name="operator_id" class="form-control" id="exampleInputPassword1" placeholder="Input Persediaan">
        </div>
      </div>
            
      <!-- /.box-body -->

      <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
  </div>

  @endsection