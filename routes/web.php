<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
|--------------------------------------------------------------------------
| LOGIN
|--------------------------------------------------------------------------
|*/

Route::get('/', function() {
    return redirect('/login');
});

Auth::routes();

route::group(['middleware' => ['auth']],function () {

            /*
            |--------------------------------------------------------------------------
            | DASHBOARD
            |--------------------------------------------------------------------------
            |*/

            Route::get('/dashboard', 'DashboardController@index');

            /*
            |--------------------------------------------------------------------------
            | CRUD BUKU
            |--------------------------------------------------------------------------
            |*/

            Route::get('/buku', 'BukuController@index');
            Route::get('/buku/create', 'BukuController@create');
            Route::post('/buku/create', 'BukuController@store');
            Route::get('/buku/edit/{id}', 'BukuController@edit');
            Route::post('/buku/edit/{id}', 'BukuController@update');
            Route::get('/buku/delete/{id}', 'BukuController@delete');

            /*
            |--------------------------------------------------------------------------
            | CRUD KATEGORI BUKU
            |--------------------------------------------------------------------------
            |*/

            Route::get('/KategoriBuku', 'KategoriBukuController@index');
            Route::get('/KategoriBuku/create', 'KategoriBukuController@create');
            Route::post('/KategoriBuku/create', 'KategoriBukuController@store');
            Route::get('/KategoriBuku/edit/{id}', 'KategoriBukuController@edit');
            Route::post('/KategoriBuku/edit/{id}', 'KategoriBukuController@update');
            Route::get('/KategoriBuku/delete/{id}', 'KategoriBukuController@delete');

            /*
            |--------------------------------------------------------------------------
            | CRUD PEMINJAMAN
            |--------------------------------------------------------------------------
            |*/

            Route::get('/Peminjaman', 'PeminjamanController@index');
            Route::get('/Peminjaman/create', 'PeminjamanController@create');
            Route::post('/Peminjaman/create', 'PeminjamanController@store');
            Route::get('/Peminjaman/edit/{id}', 'PeminjamanController@edit');
            Route::post('/Peminjaman/edit/{id}', 'PeminjamanController@update');
            Route::get('/Peminjaman/delete/{id}', 'PeminjamanController@delete');

            /*
            |--------------------------------------------------------------------------
            | CRUD DETAIL PEMINJAMAN
            |--------------------------------------------------------------------------
            |*/

            Route::get('/DetailPeminjaman', 'DetailPeminjamanController@index');
            Route::get('/DetailPeminjaman/create', 'DetailPeminjamanController@create');
            Route::post('/DetailPeminjaman/create', 'DetailPeminjamanController@store');
            Route::get('/DetailPeminjaman/edit/{id}', 'DetailPeminjamanController@edit');
            Route::post('/DetailPeminjaman/edit/{id}', 'DetailPeminjamanController@update');
            Route::get('/DetailPeminjaman/delete/{id}', 'DetailPeminjamanController@delete');

            /*
            |--------------------------------------------------------------------------
            | CRUD USER
            |--------------------------------------------------------------------------
            |*/

            Route::get('/Member', 'MemberController@index');
            Route::get('/Member/create', 'MemberController@create');
            Route::post('/Member/create', 'MemberController@store');
            Route::get('/Member/edit/{id}', 'MemberController@edit');
            Route::post('/Member/edit/{id}', 'MemberController@update');
            Route::get('/Member/delete/{id}', 'MemberController@delete');

            /*
            |--------------------------------------------------------------------------
            | CRUD MEMBER
            |--------------------------------------------------------------------------
            |*/

            Route::get('/Member', 'MemberController@index');
            Route::get('/Member/create', 'MemberController@create');
            Route::post('/Member/create', 'MemberController@store');
            Route::get('/Member/edit/{id}', 'MemberController@edit');
            Route::post('/Member/edit/{id}', 'MemberController@update');
            Route::get('/Member/delete/{id}', 'MemberController@delete');
        
        });