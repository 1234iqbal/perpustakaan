<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\KategoriBuku;

class KategoriBukuController extends Controller
{
    public function index(){
        $kategori = KategoriBuku::get();
        return view ('kategori.index',['kategori' => $kategori]);
    }

    public function create(){
        return view('kategori.create') ;
    }

    public function store(Request $request){
        $kategori = KategoriBuku::create([
            'nama' => $request->nama
        ]);
        return redirect (url('/KategoriBuku'));
    }

    public function edit($id){
        $kategori = KategoriBuku::find($id);

        return view('kategori.edit', ['kategori' => $kategori]);
    }

    public function update(Request $request, $id){
        $kategori = KategoriBuku::find($id);
        $kategori->nama = $request->nama;
        $kategori->save();
        
        return redirect (url('/KategoriBuku'));
    }

    public function delete($id) {
        $kategori = KategoriBuku::find($id);
        $kategori->delete();

        return redirect(url('/KategoriBuku'));
    }
}
