<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Buku;

class BukuController extends Controller
{
    public function index (){
        $buku = buku::get();
        return view ('buku.index',['buku' => $buku]);
    }

    public function create(){
        return view('buku.create') ;
    }

    public function store(Request $request){
        $buku = buku::create([
            'gambar' => $request->gambar,
            'judul' => $request->judul,
            'deskripsi' => $request->deskripsi,
            'pengarang' => $request->pengarang,
            'penerbit' => $request->penerbit,
            'tahun_terbit' => $request->tahun_terbit,
            'persediaan' => $request->persediaan,
            'kategori_id' => $request->kategori_id,
            'operator_id' => $request->operator_id,
        ]);
        return redirect (url('/buku'));
    }

    public function edit($id){
        $buku = buku::find($id);

        return view('buku.edit', ['buku' => $buku]);
    }

    public function update(Request $request, $id){
        $buku = buku::find($id);
        $buku->gambar = $request->gambar;
        $buku->judul = $request->judul;
        $buku->deskripsi = $request->deskripsi;
        $buku->pengarang = $request->pengarang;
        $buku->penerbit = $request->penerbit;
        $buku->tahun_terbit = $request->tahun_terbit;
        $buku->persediaan = $request->persediaan;
        $buku->kategori_id = $request->kategori_id;
        $buku->operator_id = $request->operator_id;
        $buku->save();

        return redirect (url('/buku'));
    }

    public function delete($id) {
        $buku = buku::find($id);
        $buku->delete();

        return redirect(url('/buku'));
    }
}
