<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailPeminjaman extends Model
{
    protected $table = 'detail_peminjaman';
    protected $fillable = [
        'id', 'peminjaman_id', 'buku_id', 'jumlah'];
}
