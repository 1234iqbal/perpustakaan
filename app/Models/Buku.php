<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    protected $table = 'bukus';
    
    protected $fillable = [
        'id', 
        'gambar', 
        'judul', 
        'deskripsi', 
        'pengarang', 
        'penerbit', 
        'tahun_terbit', 
        'persediaan', 
        'kategori_id', 
        'operator_id'
    ];

    public function kategori()
    {
        return $this->belongsTo('App\Models\KategoriBuku');
    }
}
