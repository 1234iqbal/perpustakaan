<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KategoriBuku extends Model
{
    protected $table = 'kategori_bukus';
    protected $fillable = [
        'id', 
        'nama',
    ];


    public function buku()
    {
    return $this->hasMany('App\Model\Buku');
    }
}