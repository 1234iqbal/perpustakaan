<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Peminjaman extends Model
{
    protected $table = 'peminjaman';
    protected $fillable = [
        'id', 
        'member_id', 
        'denda', 
        'tanggal_pinjam', 
        'tanggal_kembali', 
        'status'
    ];
}
