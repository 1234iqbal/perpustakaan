<?php

use Illuminate\Database\Seeder;

class BukuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('bukus')->insert([
        	'gambar' => 'cover.jpg',
        	'judul' => 'matematika itu sulit',
            'deskripsi' => 'buku matematika khusus untuk pemula',
            'pengarang' => 'Rini Sempoa',
        	'penerbit' => 'Erlangga',
            'tahun_terbit' => '1998',
            'persediaan' => '24',
        	'kategori_id' => '1',
            'operator_id' => '1'
        ]);
    }
}
