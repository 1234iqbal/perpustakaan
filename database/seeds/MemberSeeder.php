<?php

use Illuminate\Database\Seeder;

class MemberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('members')->insert([
        	'nama' => 'Johny',
        	'alamat' => 'Semarang',
        	'telepon' => '08997868547',
        	'info' => 'Pelajar'
        ]);
    }
}
